#!/bin/bash


set -e

mkdir building && cd building
stylelint ../builder/main.css --config ../builder/stylelint.json || exit 1
postcss -c ../builder/postcss.config.js ../builder/main.css > ./main.css

for mark in $(ls -1 ../markdown | grep md | grep -v "404.md")
do
    name=$(echo $mark | cut -d'.' -f1)
    pandoc -f markdown-auto_identifiers -t html5 --lua-filter="../builder/auto_identifiers_underscore.lua" ../markdown/$name.md -o body.html
    cat ../markdown/$mark | head -n1 | sed -E "s|^#\s||g;s|\s$||g" > title
    cat ../builder/header.html body.html ../builder/footer.html | m4 > $name-premini.html
    html-minifier -c ../builder/html-minifier.conf $name-premini.html > $name.html
    rm body.html $name-premini.html title
done

# 404
stylelint ../builder/404.css --config ../builder/stylelint.json || exit 1
postcss -c ../builder/postcss.config.js ../builder/404.css > ./404.css
pandoc -f markdown-auto_identifiers -t html5 --lua-filter="../builder/auto_identifiers_underscore.lua" ../markdown/404.md -o 404_body.html
m4 ../builder/404_prem4.html > 404_m4ed.html
html-minifier -c ../builder/html-minifier.conf 404_m4ed.html > 404.html

cp ../builder/netlify.toml .
rm 404_body.html 404_m4ed.html main.css 404.css

# Deploy
cd ..
mkdir mehranbaghicom && cd mehranbaghicom
wget https://gitlab.com/libre_vad/book/-/jobs/artifacts/master/download?job=website
mv download\?job\=website libre_vad.zip
unzip libre_vad.zip
rm libre_vad.zip
mkdir libre_vad
mv building/artifacts/* libre_vad
rm -rf building
wget https://gitlab.com/hott_for_cools/book/-/jobs/artifacts/master/download?job=website
mv download\?job\=website hott_for_cools.zip
unzip hott_for_cools.zip
rm hott_for_cools.zip
mkdir hott_for_cools
mv building/artifacts/* hott_for_cools
rm -rf building
cp ../building/* .
cd ..
zip -r website.zip mehranbaghicom
curl -H "Content-Type: application/zip" -H "Authorization: Bearer ${NETLIFY_TOKEN}" --data-binary "@website.zip" https://api.netlify.com/api/v1/sites/${NETLIFY_ID}/deploys > /dev/null 2>&1