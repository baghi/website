# I Know Nothing

My name is Mehran Baghi and this is my personal website.


## My Projects:

* [HoTT for Cools](./hott_for_cools/): This is a book I'm currently
  working on. It consists of notes that I'm taking from a book called
  "[Homotopy Type Theory: Univalent Foundations of
  Mathematics](https://github.com/HoTT/book)"

* [Libre VAD](./libre_vad/): I'm doing some research on [Ventricular
  Assist Device](https://en.wikipedia.org/wiki/Ventricular_assist_device). I
  want to design one and release everything under a Libre/Open source license.
  

## You can find me on:

* [YouTube](https://www.youtube.com/channel/UCmiJUcZJxHQgEmY2v6nXubQ)
* [GitLab](https://gitlab.com/baghi) (My main git host)
* [GitHub](https://github.com/Mehran-Baghi)
* Gmail with this address: 1mehranbaghi
